# Building the docker image

You can build the provided Dockerfile into a Docker image with the command below:

`docker build -t <image_name> .`

Alternatively, you can pull the docker image hosted within this repo with the command below:

`docker pull registry.gitlab.com/k3y/sancus-pfcp-attacks:latest`

you can also use the corresponding GitHub registry:

`docker pull ghcr.io/g-ampo/sancus-pfcp-attacks:latest`

# Launching a Docker container

To create a Docker container from the Docker image, you can use the docker run command. The general syntax of the command is as follows:

Assuming you want to run the Session Deletion or Modification scripts:
`docker run -it <image_name> <script_name> <IP_src> <IP_dst> <Interface>`

Alternatively, assuming you want to run the Session Establishment Flood script:

`docker run -it <image_name> <script_name> <IP_src> <IP_dst> <N3_IP_V4> <GNB_IP_V4>`

Replace `<image_name>` with the name of the built Docker image, and `<script_name>`, `<IP_src>`, `<IP_dst>`, `<Interface>`, `<N3_IP_V4>`, and `<GNB_IP_V4>` with the appropriate values.

For example, to run the `pfcp-SD-DoS.py` script with `192.168.1.1` as the source IP address, `192.168.1.16` as the destination IP address, and `eth0` as the network interface, you would run the following command:

`docker run -it python_scripts pfcp-SD-DoS.py 192.168.1.1 192.168.1.1 eth0`

Similarly to run the `pfcp-SM-DoS.py` script with `192.168.1.1` as the source IP address, `192.168.1.16` as the destination IP address, and `eth0` as the network interface, you would run the following command:

`docker run -it python_scripts pfcp-SM-DoS.py 192.168.1.1 192.168.1.1 eth0`

To run the pfcp-Flood.py script with `192.168.1.1` as the source IP address, `192.168.1.16` destination IP address, `192.168.1.2` as N3_IP_V4, and `192.168.1.3` as `GNB_IP_V4`, you would run the following command:

`docker run -it python_scripts pfcp-Flood.py 192.168.1.1 192.168.1.1 192.168.1.2 192.168.1.3`

Overall, you need to replace `<script_name>`, `<IP_src>`, `<IP_dst>`, `<Interface>`, `<N3_IP_V4>`, and `<GNB_IP_V4>` with the appropriate values, and `<image_name>` with the name of the built Docker image.